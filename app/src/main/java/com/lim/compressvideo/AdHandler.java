package com.lim.compressvideo;

import android.content.Context;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

/**
 * Created by Lenovo on 03-02-2018.
 */

public class AdHandler {
    private static AdHandler adHandler;
    private static InterstitialAd mInterstitialAd;
    public static AdHandler getInstance() {
        if (adHandler == null) {
            adHandler = new AdHandler();
        }

        return adHandler;
    }

    public static AdRequest loadAd() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        return adRequest;
    }

    public static void loadFullScreenAd(Context context) {
        mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(context.getString(R.string.admob_fullscreen_ad));
        mInterstitialAd.loadAd(loadAd());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();

                mInterstitialAd.loadAd(loadAd());
            }
        });
    }

    public static void showFullScrenAd() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    public void loadBannerAd(final AdView adView, Context context){
        AdListener listener = new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.setVisibility(View.VISIBLE);
            }
        };

        adView.setAdListener(listener);
        adView.loadAd(loadAd());
    }

}