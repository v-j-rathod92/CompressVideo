package com.lim.compressvideo;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdView;
import com.lim.compressvideo.permission.GGPermissionManager;
import com.lim.compressvideo.permission.OnRequestPermissionsCallBack;
import com.lim.compressvideo.permission.PermissionRequest;
import com.lim.compressvideo.permission.util.PathUtil;

import java.net.URISyntaxException;
import java.util.List;

import hotchemi.android.rate.AppRate;
import hotchemi.android.rate.OnClickButtonListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private List<Uri> mSelected;
    private Context mContext;
    private static final int TAKE_GALLERY_VIDEO = 101;
    private CardView cardStart;
    private AdView adView;
    private AdHandler adHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        setListeners();
    }

    private void init() {
        Log.e("package", getPackageName());
        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        cardStart = findViewById(R.id.cardStart);
        adView = findViewById(R.id.adView);

        loadAds();
        launchRateDialog();
    }

    private void loadAds() {
        adHandler = AdHandler.getInstance();
        adHandler.loadBannerAd(adView, this);
        adHandler.loadFullScreenAd(this);
    }

    private void setListeners() {
        cardStart.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == TAKE_GALLERY_VIDEO) {
            Uri selectedImageUri = data.getData();
            String selectedImagePath;
            // OI FILE Manager

            // MEDIA GALLERY
            try {
                selectedImagePath = PathUtil.getPath(this, selectedImageUri);
                Log.e("path", selectedImagePath);

                Intent intent = new Intent(mContext, CompressVideoActivity.class);
                intent.putExtra(CompressVideoActivity.VIDEO_URI, selectedImagePath);
                startActivity(intent);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }


    private void pickMediaFile() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Video"), TAKE_GALLERY_VIDEO);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cardStart:
                new GGPermissionManager.Builder(mContext)
                        .addPermissions(PermissionRequest.readWriteStoragePermission()) //change permission as require
                        .addRequestPermissionsCallBack(new OnRequestPermissionsCallBack() {
                            @Override
                            public void onGrant() {
                                pickMediaFile();
                            }

                            @Override
                            public void onDenied(String permission) {
                                Log.e("permission", "denied");
                            }

                        }).build().request();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.share) {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Compress Video \n " +
                    "https://play.google.com/store/apps/details?id=" + getPackageName());
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, "Share App"));
        } else if (item.getItemId() == R.id.rate) {

            Uri uri = Uri.parse("market://details?id=" + getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void launchRateDialog() {
        AppRate.with(this)
                .setInstallDays(0) // after 0 days of install, default 10
                .setLaunchTimes(10) // after 3 time launch, default 1
                .setRemindInterval(2) // after 2 days if user select remind me later, default 1
                .setShowLaterButton(true) // default true
                .setDebug(false) // default false
                .setOnClickButtonListener(new OnClickButtonListener() { // callback listener.
                    @Override
                    public void onClickButton(int which) {

                    }
                })
                .monitor();

        // Show a dialog if meets conditions
        AppRate.showRateDialogIfMeetsConditions(this);
    }
}
