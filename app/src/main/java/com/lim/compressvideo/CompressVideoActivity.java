package com.lim.compressvideo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by rgi-40 on 22/2/18.
 */

public class CompressVideoActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "COMMAND";
    public static final String VIDEO_URI = "video_uri";
    private VideoView videoView;
    private TextView txtStart, txtEnd;
    private SeekBar seekBar;
    private Runnable runVideo = new RunVideo();
    private Handler handler = new Handler();
    private boolean isPlay = false;
    private int duration = 0;
    private FFmpeg ffmpeg;

    private Button btnCompress, btnLow, btnMedium, btnHigh;
    private int QUALITY;
    private String sourceVideoPath;
    private String timeRe = "\\btime=\\b\\d\\d:\\d\\d:\\d\\d.\\d\\d";
    int last = 0;
    private float toatalSecond;
    double percen = 0.0d;
    private ProgressDialog dialog;
    private MaterialPlayPauseButton playPause;
    private String destinationPath;

    private AdView adView;
    private AdHandler adHandler;

    class RunVideo implements Runnable {
        @Override
        public void run() {
            if (videoView.isPlaying()) {
                int curPos = videoView.getCurrentPosition();
                seekBar.setProgress(curPos);

                try {
                    txtStart.setText(formatTimeUnit((long) curPos));

                    if (curPos == duration) {
                        seekBar.setProgress(0);
                        txtStart.setText("00:00");
                        playPause.setToPlay();
                        handler.removeCallbacks(runVideo);
                        return;
                    }

                    handler.postDelayed(runVideo, 500);
                    return;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            try {
                txtStart.setText(formatTimeUnit((long) duration));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            handler.removeCallbacks(runVideo);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_compress_video);

        init();
        setListeners();


    }

    private void init() {
        videoView = findViewById(R.id.videoView);
        txtStart = findViewById(R.id.txtStart);
        txtEnd = findViewById(R.id.txtEnd);
        seekBar = findViewById(R.id.seekBar);

        btnCompress = findViewById(R.id.btnCompress);
        btnLow = findViewById(R.id.btnLow);
        btnMedium = findViewById(R.id.btnMedium);
        btnHigh = findViewById(R.id.btnHigh);

        playPause = findViewById(R.id.playPause);
        adView = findViewById(R.id.adView);

        playPause.setColor(ContextCompat.getColor(this, R.color.colorPrimary));
        playPause.setAnimDuration(300);


        sourceVideoPath = getIntent().getStringExtra(VIDEO_URI);

        if (sourceVideoPath != null && !sourceVideoPath.equals("")) {
            videoView.setVideoURI(Uri.parse(sourceVideoPath));
        }

        videoView.seekTo(100);
        changeQuality(2);
        loadFFMpegBinary();
        loadAds();
    }

    private void setListeners() {
        playPause.setOnClickListener(this);
        btnCompress.setOnClickListener(this);
        btnLow.setOnClickListener(this);
        btnMedium.setOnClickListener(this);
        btnHigh.setOnClickListener(this);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                duration = videoView.getDuration();
                seekBar.setMax(duration);
                txtStart.setText("00:00");
                try {
                    txtEnd.setText(formatTimeUnit((long) duration));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playPause.setToPlay();
                videoView.seekTo(100);
                seekBar.setProgress(0);
                txtStart.setText("00:00");
                handler.removeCallbacks(runVideo);
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                videoView.seekTo(progress);
                try {
                    txtStart.setText(formatTimeUnit((long) progress));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.playPause:
                if (videoView.isPlaying()) {
                    videoView.pause();
                    handler.removeCallbacks(runVideo);
                    playPause.setToPlay();
                } else {
                    videoView.seekTo(seekBar.getProgress());
                    videoView.start();
                    handler.postDelayed(runVideo, 500);
                    playPause.setToPause();
                }
                isPlay = !isPlay;
                break;

            case R.id.btnLow:
                changeQuality(1);
                break;

            case R.id.btnMedium:
                changeQuality(2);
                break;

            case R.id.btnHigh:
                changeQuality(3);
                break;

            case R.id.btnCompress:
                dialog = new ProgressDialog(this);
                dialog.setMessage("Compressing video.. " + String.format("%.0f", new Object[]{Double.valueOf(percen)}) + "%");
                dialog.setCancelable(false);
                dialog.show();
                compressVideo();
                break;
        }
    }

    private void changeQuality(int quality) {
        if (quality == 1) {
            QUALITY = 1;
            btnLow.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            btnMedium.setTextColor(ContextCompat.getColor(this, android.R.color.black));
            btnHigh.setTextColor(ContextCompat.getColor(this, android.R.color.black));

            btnLow.setTypeface(btnLow.getTypeface(), Typeface.BOLD);
            btnMedium.setTypeface(btnMedium.getTypeface(), Typeface.NORMAL);
            btnHigh.setTypeface(btnHigh.getTypeface(), Typeface.NORMAL);
        } else if (quality == 2) {
            QUALITY = 2;
            btnLow.setTextColor(ContextCompat.getColor(this, android.R.color.black));
            btnMedium.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            btnHigh.setTextColor(ContextCompat.getColor(this, android.R.color.black));

            btnLow.setTypeface(btnLow.getTypeface(), Typeface.NORMAL);
            btnMedium.setTypeface(btnMedium.getTypeface(), Typeface.BOLD);
            btnHigh.setTypeface(btnHigh.getTypeface(), Typeface.NORMAL);
        } else if (quality == 3) {
            QUALITY = 3;
            btnLow.setTextColor(ContextCompat.getColor(this, android.R.color.black));
            btnMedium.setTextColor(ContextCompat.getColor(this, android.R.color.black));
            btnHigh.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));

            btnLow.setTypeface(btnLow.getTypeface(), Typeface.NORMAL);
            btnMedium.setTypeface(btnMedium.getTypeface(), Typeface.NORMAL);
            btnHigh.setTypeface(btnHigh.getTypeface(), Typeface.BOLD);
        }
    }

    private void compressVideo() {
        int qlty = 0;
        if (QUALITY == 1) {
            qlty = 20;
        } else if (QUALITY == 2) {
            qlty = 25;
        } else if (QUALITY == 3) {
            qlty = 29;
        }
        File moviesDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES
        );

        String filePrefix = "compress_video";
        String fileExtn = ".mp4";

        File dest = new File(moviesDir, filePrefix + fileExtn);
        int fileNo = 0;
        while (dest.exists()) {
            fileNo++;
            dest = new File(moviesDir, filePrefix + fileNo + fileExtn);
        }

        Log.d(TAG, "startTrim: dest: " + dest.getAbsolutePath());
        destinationPath = dest.getAbsolutePath();

        execFFmpegBinary(new String[]{"-y", "-i", sourceVideoPath, "-vcodec", "libx264", "-crf", new StringBuilder(String.valueOf(qlty)).toString(), "-acodec", "copy", "-preset", "veryfast", destinationPath});
    }

    public static String formatTimeUnit(long millis) throws ParseException {
        return String.format("%02d:%02d", new Long[]{Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(millis)), Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))});
    }

    private void loadFFMpegBinary() {
        try {
            if (ffmpeg == null) {
                Log.d(TAG, "ffmpeg : era nulo");
                ffmpeg = FFmpeg.getInstance(this);
            }
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    showUnsupportedExceptionDialog();
                }

                @Override
                public void onSuccess() {
                    Log.d(TAG, "ffmpeg : correct Loaded");
                }
            });
        } catch (FFmpegNotSupportedException e) {
            showUnsupportedExceptionDialog();
        } catch (Exception e) {
            Log.d(TAG, "EXception no controlada : " + e);
        }
    }

    private void loadAds() {
        adHandler = AdHandler.getInstance();
        adHandler.loadBannerAd(adView, this);
        adHandler.showFullScrenAd();
    }

    private void execFFmpegBinary(final String[] command) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
//                    Log.d(TAG, "FAILED with output : " + s);
                    dialog.dismiss();
                }

                @Override
                public void onSuccess(String s) {
//                    Log.d(TAG, "Success command : ffmpeg " + s);
                    dialog.dismiss();
                    showOpenFileDialog();
                }

                @Override
                public void onProgress(String s) {
//                    Log.d(TAG, "Started command : ffmpeg " + s);
                    durationToprogtess(s);
                }

                @Override
                public void onStart() {
//                    Log.d(TAG, "Started command : ffmpeg " + command);

                }

                @Override
                public void onFinish() {
//                    Log.d(TAG, "Finished command : ffmpeg " + command);

                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // do nothing for now
        }
    }

    private void showUnsupportedExceptionDialog() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Not Supported")
                .setMessage("Device Not Supported")
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .create()
                .show();

    }

    private void showOpenFileDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setTitle("Video Compressed")
                .setMessage("Your compressed video has been stored in \"" + destinationPath + "\"")
                .setCancelable(false)
                .setPositiveButton("Open", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(destinationPath));
                        intent.setDataAndType(Uri.parse(destinationPath), "video/mp4");
                        startActivity(Intent.createChooser(intent, "Open Video"));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        dialog.show();

    }

    private int durationToprogtess(String input) {
        int progress = 0;
        Matcher matcher = Pattern.compile(this.timeRe).matcher(input);
        int MINUTE = 1 * 60;
        int HOUR = MINUTE * 60;
        if (TextUtils.isEmpty(input) || !input.contains("time=")) {
//            Log.e("time", "not contain time " + input);
            return this.last;
        }
        while (matcher.find()) {
            String time = matcher.group();
            String[] splitTime = time.substring(time.lastIndexOf(61) + 1).split(":");
            float hour = ((Float.valueOf(splitTime[0]).floatValue() * ((float) HOUR)) + (Float.valueOf(splitTime[1]).floatValue() * ((float) MINUTE))) + Float.valueOf(splitTime[2]).floatValue();
//            Log.e("time", "totalSecond:" + hour);
            this.toatalSecond = (float) Integer.parseInt(String.valueOf(this.videoView.getDuration() / 1000));
            progress = (int) ((100.0f * hour) / this.toatalSecond);
            updateInMili(hour);
        }
        this.last = progress;
        return progress;
    }

    private void updateInMili(final float time) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                percen = (((double) time) * 100.0d) / ((double) toatalSecond);
                if (percen + 15.0d > 100.0d) {
                    percen = 100.0d;
                } else {
                    CompressVideoActivity viewVideo = CompressVideoActivity.this;
                    viewVideo.percen += 15.0d;
                }
                dialog.setMessage("Compressing video.. " + String.format("%.0f", new Object[]{Double.valueOf(percen)}) + "%");
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        adHandler.showFullScrenAd();
    }
}
